@import CoreGraphics;
@import Cocoa;
@import Foundation;
@import AppKit;

#import "SGLibX.h"

#define ALL_EVENTS_MASK 0xFFFFFFFF

struct mouse {
    int x, y;
};

@implementation SGCanvas {
    CGContextRef cxt_data;
}

- (id)init {
    if (self = [super init]) {
        cxt_data = [[NSGraphicsContext currentContext] graphicsPort];
        CGContextFlush(cxt_data);
        CGContextSetStrokeColorWithColor(cxt_data, CGColorCreateGenericRGB(1.0, 1.0, 1.0, 1.0));
    }
    
    return self;
}

- (void)setColor: (UInt32)color {
    CGContextSetStrokeColorWithColor(cxt_data, CGColorCreateGenericRGB(RGBA_R(color), RGBA_G(color),
                                                                       RGBA_B(color), RGBA_A(color)));
}

- (void)drawPoint: (int)x : (int)y {
    CGContextFillRect(cxt_data, CGRectMake(x, y, 1, 1));
}

- (void)drawLine: (int)x0 : (int)y0 : (int)x1 : (int)y1 {
    CGContextBeginPath(cxt_data);
    CGContextMoveToPoint(cxt_data, x0, y0);
    CGContextAddLineToPoint(cxt_data, x1, y1);
    CGContextStrokePath(cxt_data);
}

- (void)drawRect: (int)x0 : (int)y0 : (int)x1 : (int)y1 {
    CGRect rect = CGRectMake(x0, y0, x1 - x0, y1 - y0);
    rect = CGRectStandardize(rect);
    CGContextFillRect(cxt_data, rect);
}

- (void)drawEllipse: (int)x0 : (int)y0 : (int)x1 : (int)y1 {
    CGContextFillEllipseInRect(cxt_data, CGRectStandardize(CGRectMake(x0, y0, x1, y1)));
}

- (void)drawCircle: (int)x : (int)y : (int)r {
    [self drawEllipse: x - r : y - r : x + r : y + r];
}

- (void)drawArc: (int)x0 : (int)y0 : (int)x1 : (int)y1 : (int)begin : (int)end {
    int height = abs(y1 - y0);
    int width = abs(x1 - x0);
    
    CGFloat cx = (x0 + x1) / 2;
    CGFloat cy = (y0 + y1) / 2;
    CGFloat r = width;
    
    CGMutablePathRef path = CGPathCreateMutable();
    CGAffineTransform transform = CGAffineTransformMakeTranslation(cx, cy);
    transform = CGAffineTransformConcat(CGAffineTransformMakeScale(1.0, (width / height)), transform);
    CGPathAddArc(path, &transform, 0, 0, r, begin, end, false);
    CGContextAddPath(cxt_data, path);
    
    CGContextStrokePath(cxt_data);
    
    CFRelease(path);
}

- (void)drawPie: (int)x0 : (int)y0 : (int)x1 : (int)y1 : (int)begin : (int)end {
    int height = abs(y1 - y0);
    int width = abs(x1 - x0);
    
    CGFloat cx = (x0 + x1) / 2;
    CGFloat cy = (y0 + y1) / 2;
    CGFloat r = width;
    
    CGMutablePathRef path = CGPathCreateMutable();
    CGAffineTransform transform = CGAffineTransformMakeTranslation(cx, cy);
    transform = CGAffineTransformConcat(CGAffineTransformMakeScale(1.0, (width / height)), transform);
    CGPathAddArc(path, &transform, 0, 0, r, begin, end, false);
    CGContextAddPath(cxt_data, path);
    
    // Insert radiuses...
    
    CGContextStrokePath(cxt_data);
    
    CFRelease(path);
}

- (void)drawChord: (int)x0 : (int)y0 : (int)x1 : (int)y1 : (int)begin : (int)end {
    // Draw it...
}

- (void)drawPolygon: (struct SGPoint *)vertices : (int)n_vert {
    CGMutablePathRef (^makePolygonPath)() = ^() {
        CGMutablePathRef res = CGPathCreateMutable();
        
        CGPathMoveToPoint(res, nil, vertices[0].x, vertices[0].y);
        for (int i = 1; i < n_vert; i++)
            CGPathAddLineToPoint(res, nil, vertices[i].x, vertices[i].y);
        
        CGPathCloseSubpath(res);
        
        return res;
    };
    
    CGContextAddPath(cxt_data, makePolygonPath());
    CGContextStrokePath(cxt_data);
}

- (void)dealloc {
    CGContextRelease(cxt_data);
}
@end

@implementation SGWindow {
    NSWindow *wnd;
    bool running;
}

- (id)init {
    if (self = [super init]) {
        wnd = [NSWindow alloc];
        running = false;
    }
    
    return self;
}

- (void)create: (int)width : (int)height : (NSString *)title : (bool)centered {
    wnd = [wnd initWithContentRect: NSMakeRect(0, 0, width, height)
                         styleMask: (NSTitledWindowMask | NSResizableWindowMask |
                                     NSMiniaturizableWindowMask | NSClosableWindowMask)
                           backing: NSBackingStoreBuffered
                             defer: false];
    
    if (centered)
        [wnd center];
    
    running = true;
    
    [wnd setTitle: title];
    [wnd makeKeyAndOrderFront: self];
    
    while (true) {
        NSEvent *ev = [wnd currentEvent];
        
        switch ([ev type]) {
            case NSLeftMouseUp: {
                mouse_data.mb &= ~0x4;
                break;
            }
                
            case NSLeftMouseDown: {
                mouse_data.mb |= 0x4;
                break;
            }
                
            case NSRightMouseUp: {
                mouse_data.mb &= ~0x2;
                break;
            }
                
            case NSRightMouseDown: {
                mouse_data.mb |= 0x2;
                break;
            }
                
            case NSMouseMoved: {
                mouse_data.coord.x = [NSEvent mouseLocation].x -
                                     [NSWindow contentRectForFrameRect: [wnd frame]
                                                             styleMask: style].origin.x;
                mouse_data.coord.y = [NSEvent mouseLocation].x -
                                     [NSWindow contentRectForFrameRect: [wnd frame]
                                                             styleMask: style].origin.y;
                break;
            }
            
            default:
                break;
        }
        
        [wnd nextEventMatchingMask: ALL_EVENTS_MASK];
    }
}

- (void)destroy {
    [wnd close];
    running = false;
}

- (void)dealloc {
    [self destroy];
}
@end

CGFloat sgGetScreenWidth() {
    return [[NSScreen mainScreen] frame].size.width;
}

CGFloat sgGetScreenHeight() {
    return [[NSScreen mainScreen] frame].size.height;
}

UInt32 RGBA(UInt32 r, UInt32 g, UInt32 b, UInt32 a) {
    return ((r & 0xFF) |
            ((g & 0xFF) << 0x8) |
            ((b & 0xFF) << 0x10) |
            ((a & 0xFF) << 0x18));
}

CGFloat RGBA_R(UInt32 clr) {
    return (CGFloat)(clr & 0xFF) / 0xFF;
}

CGFloat RGBA_G(UInt32 clr) {
    return (CGFloat)((clr >> 0x8) & 0xFF) / 0xFF;
}

CGFloat RGBA_B(UInt32 clr) {
    return (CGFloat)((clr >> 0x10) & 0xFF) / 0xFF;
}

CGFloat RGBA_A(UInt32 clr) {
    return (CGFloat)((clr >> 0x18) & 0xFF) / 0xFF;
}