@import Cocoa;
@import CoreGraphics;
@import Foundation;

FOUNDATION_EXPORT double SGLibXVersionNumber;
FOUNDATION_EXPORT const unsigned char SGLibXVersionString[];

struct SGPoint {
    int x;
    int y;
};

@interface SGCanvas: NSObject
- (id)init;
- (void)setColor: (UInt32)clr;
- (void)drawPoint: (int)x : (int)y;
- (void)drawLine: (int)x0 : (int)y0 : (int)x1 : (int)y1;
- (void)drawRect: (int)x0 : (int)y0 : (int)x1 : (int)y1;
- (void)drawEllipse: (int)x0 : (int)y0 : (int)x1 : (int)y1;
- (void)drawCircle: (int)x0 : (int)y0 : (int)r;
- (void)drawArc: (int)x0 : (int)y0 : (int)x1 : (int)y1 : (int)begin : (int)end;
- (void)drawPie: (int)x0 : (int)y0 : (int)x1 : (int)y1 : (int)begin : (int)end;
- (void)drawChord: (int)x0 : (int)y0 : (int)x1 : (int)y1 : (int)begin : (int)end;
- (void)drawPolygon: (struct SGPoint *)vertices : (int)n_vert;
- (void)dealloc;
@end

@interface SGWindow: NSObject
- (id)init;
- (void)create: (int)width : (int)height : (NSString *)title : (bool)centered;
- (void)destroy;
- (void)dealloc;
@end

CGFloat sgGetScreenWidth();
CGFloat sgGetScreenHeight();
UInt32 RGBA(UInt32 r, UInt32 g, UInt32 b, UInt32 a);
CGFloat RGBA_R(UInt32 clr);
CGFloat RGBA_G(UInt32 clr);
CGFloat RGBA_B(UInt32 clr);
CGFloat RGBA_A(UInt32 clr);