@import Cocoa;
@import XCTest;

#import "SGLibX.h"

void delay(double time) {
    [NSThread sleepForTimeInterval: time];
}

@interface SGLibXTests : XCTestCase

@end

@implementation SGLibXTests
- (void)setUp {
    [super setUp];
}

- (void)tearDown {
    [super tearDown];
}

- (void)testExample {
    @try {
        [NSApplication sharedApplication];
        
        SGWindow *wnd = [[SGWindow alloc] init];
        [wnd create: 800 : 640 : @"Test window" : true];
        delay(3);
        [wnd destroy];
    } @catch (NSException *exc) {
        NSLog(@"Exception occurred: %@", exc);
        @throw exc;
    }
    
    XCTAssert(YES, @"Pass");
}

- (void)testPerformanceExample {
    [self measureBlock:^{
    }];
}

@end
